//
//  PresetTVCell.swift
//  Maitreya
//
//  Created by Michael Nikolaev on 02.09.17.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import UIKit

class PresetTVCell: UITableViewCell {

    var delegate: PresetDelegate!
    
    var preventDeleting = false
    
    var preset: Preset! {
        didSet {
            nameLbl.text = preset.name
            timerLbl.text = preset.timer
        }
    }
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var timerLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    @IBAction func deletePreset(_ sender: Any) {
        if preventDeleting { return }
        delegate.deletePreset(preset: preset)
    }
}
