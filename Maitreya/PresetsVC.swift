//
//  PresetsVC.swift
//  Maitreya
//
//  Created by Michael Nikolaev on 02.09.17.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import UIKit
import RealmSwift

class Preset: Object {
    dynamic var timer: String!
    dynamic var name: String!
}

protocol PresetDelegate {
    func deletePreset(preset: Preset)
}

class PresetsVC: UIViewController, UITableViewDelegate, UITableViewDataSource, PresetDelegate {

    @IBOutlet weak var bottomBarView: UIImageView!
    @IBOutlet weak var presetsTV: UITableView!
    
    var presets = [Preset]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let realm = try! Realm()
        let prs = realm.objects(Preset.self).sorted(byKeyPath: "name", ascending: true)

        if prs.count == 0 {
            presets.append(Preset(value: ["timer": "1:00", "name": "Preset1"]))
            presets.append(Preset(value: ["timer": "1:00", "name": "Preset2"]))
            presets.append(Preset(value: ["timer": "1:00", "name": "Preset3"]))
            presets.append(Preset(value: ["timer": "1:00", "name": "Preset4"]))
            presets.append(Preset(value: ["timer": "1:00", "name": "Preset5"]))
            presets.append(Preset(value: ["timer": "1:00", "name": "Preset6"]))
            presets.append(Preset(value: ["timer": "1:00", "name": "Preset7"]))
        
            try! realm.write {
                realm.add(presets)
            }
        } else {
            for pr in prs {
                presets.append(pr)
            }
        }
        
        presetsTV.register(UINib(nibName: "PresetTVCell", bundle: nil), forCellReuseIdentifier: "PresetCell")
        
        presetsTV.delegate = self
        presetsTV.dataSource = self

        bottomBarView.tintColor = UIColor(red: 124/255, green: 20/255, blue: 44/255, alpha: 1.0)
        presetsTV.tableFooterView = UIView()
    }
    
    func deletePreset(preset: Preset) {
        for (i, p) in presets.enumerated() {
            if p.name == preset.name && p.timer == preset.timer {
                presets.remove(at: i)
                presetsTV.reloadSections(IndexSet([0]), with: .fade)
                
                let realm = try! Realm()
                try! realm.write {
                    realm.delete(p)
                }
                break
            }
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presets.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PresetCell", for: indexPath) as? PresetTVCell
        cell?.preset = presets[indexPath.row]
        cell?.delegate = self
        if presets.count == 1 && indexPath.row == 0 {
            cell?.preventDeleting = true
        }
        
        return cell!
    }

    @IBAction func pressedFirst(_ sender: Any) {
        let controller = TimerVC(nibName: "TimerVC", bundle: nil)
        self.present(controller, animated: false, completion: nil)
    }
    
    @IBAction func pressedSecond(_ sender: Any) {
        let controller = CounterVC(nibName: "CounterVC", bundle: nil)
        self.present(controller, animated: false, completion: nil)
    }
    
    @IBAction func pressedThird(_ sender: Any) {
        let controller = PracticeVC(nibName: "PracticeVC", bundle: nil)
        self.present(controller, animated: false, completion: nil)
    }
    
    @IBAction func pressedForth(_ sender: Any) { }
}
