//
//  ViewController.swift
//  Maitreya
//
//  Created by Michael Nikolaev on 31.08.17.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var firstBtn: UIButton!
    @IBOutlet weak var secondBtn: UIButton!
    @IBOutlet weak var thirdBtn: UIButton!
    @IBOutlet weak var forthBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        firstBtn.setImage(UIImage(named: "iconTimerOn"), for: .highlighted)
        secondBtn.setImage(UIImage(named: "iconCounterOn"), for: .highlighted)
        thirdBtn.setImage(UIImage(named: "iconPracticeOn"), for: .highlighted)
        forthBtn.setImage(UIImage(named: "iconPresetOn"), for: .highlighted)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func openTimer(_ sender: Any) {
    }

    @IBAction func openCounter(_ sender: Any) {
        let controller = CounterVC(nibName: "CounterVC", bundle: nil)
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func openPractice(_ sender: Any) {
        let controller = PracticeVC(nibName: "PracticeVC", bundle: nil)
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func openPresets(_ sender: Any) {
        let controller = PresetsVC(nibName: "PresetsVC", bundle: nil)
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func openDonations(_ sender: Any) {
        let controller = CounterVC(nibName: "CounterVC", bundle: nil)
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func openWeb(_ sender: Any) {
    }
    
    func tint(image: UIImage, color: UIColor) -> UIImage
    {
        let ciImage = CIImage(image: image)
        let filter = CIFilter(name: "CIMultiplyCompositing")
        
        let colorFilter = CIFilter(name: "CIConstantColorGenerator")
        let ciColor = CIColor(color: color)
        colorFilter?.setValue(ciColor, forKey: kCIInputColorKey)
        let colorImage = colorFilter?.outputImage
        
        filter?.setValue(colorImage, forKey: kCIInputImageKey)
        filter?.setValue(ciImage, forKey: kCIInputBackgroundImageKey)
        
        let outImg = filter!.outputImage
        
        return UIImage(ciImage: outImg!)
    }

}

