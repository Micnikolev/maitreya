//
//  AddCounterVC.swift
//  Maitreya
//
//  Created by Michael Nikolaev on 06.09.17.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import UIKit

protocol AddCounterDelegate {
    func saveCounter(name: String, roundSize: String)
}

class AddCounterVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var roundSizeTF: UITextField!
    @IBOutlet weak var bottomView: UIImageView!
    
    var delegate: AddCounterDelegate!
    
    var index: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bottomView.tintColor = UIColor(red: 124/255, green: 20/255, blue: 44/255, alpha: 1.0)
        
        nameTF.delegate = self
        roundSizeTF.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func okTapped(_ sender: Any) {
        self.delegate.saveCounter(name: nameTF.text!, roundSize: roundSizeTF.text!)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
