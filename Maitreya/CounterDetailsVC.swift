//
//  CounterDetailsVC.swift
//  Maitreya
//
//  Created by Michael Nikolaev on 06.09.17.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import UIKit

class CounterDetailsVC: UIViewController {

    @IBOutlet weak var bottomView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var countLbl: UILabel!
    @IBOutlet weak var roundLbl: UILabel!
    @IBOutlet weak var totalLb: UILabel!
    @IBOutlet weak var timer: AppusCircleTimer!
    @IBOutlet weak var smallCircle: UIView!
    @IBOutlet weak var circleView: CircleView!
    
    var changedAngle: CGFloat = 0.0
    var didPress = false
    
    var count = 1
    var round = 1
    var total = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bottomView.tintColor = UIColor(red: 124/255, green: 20/255, blue: 44/255, alpha: 1.0)
        timer.totalTime = 100
        timer.elapsedTime = 20
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func okayPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func pressedOnCircle(_ sender: Any) {
        smallCircle.layer.removeAllAnimations()
        
        timer.elapsedTime = timer.elapsedTime + 20
        timer.setNeedsDisplay()
        
        let orbit = CAKeyframeAnimation(keyPath: "position")
        var affineTransform = CGAffineTransform(rotationAngle: 0.0)
        affineTransform = affineTransform.rotated(by: CGFloat(Double.pi/4))
        let circlePath =  UIBezierPath(arcCenter: CGPoint(x: circleView.bounds.origin.x+circleView.bounds.width/2,y: circleView.bounds.origin.y+circleView.bounds.height/2), radius: circleView.frame.size.width/2-10, startAngle: changedAngle, endAngle:changedAngle + CGFloat(Double.pi/4), clockwise: true)
        orbit.path = circlePath.cgPath
        orbit.duration = 0.3
        orbit.repeatCount = 1
        orbit.calculationMode = kCAAnimationPaced
        orbit.rotationMode = kCAAnimationRotateAuto
        orbit.isRemovedOnCompletion = false
        orbit.fillMode = kCAFillModeForwards
        
        smallCircle.layer.add(orbit, forKey: "orbit")
        
        changedAngle += CGFloat(Double.pi/2.5)
        
        if (timer.elapsedTime >= timer.totalTime*TimeInterval(round)) {
            count = 0
            round += 1
        } else {
            count += 1
        }
        
        total += 1
        
        countLbl.text = String(count)
        roundLbl.text = String(round-1)
        totalLb.text = String(total)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
