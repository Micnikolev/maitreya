//
//  CircleView.swift
//  Maitreya
//
//  Created by Michael Nikolaev on 10.09.17.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import UIKit

@IBDesignable
class CircleView: UIView {

    
    @IBInspectable public var color = UIColor(red: 209/255, green: 202/255, blue: 186/255, alpha: 1.0)
    
    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else {return}
        
        context.addEllipse(in: rect)
        context.setFillColor(color.cgColor)
        context.fillPath()
    }

}
