//
//  RoundedView.swift
//  Maitreya
//
//  Created by Michael Nikolaev on 02.09.17.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import UIKit

class RoundedView: UIView {
    
    var colorForBezel: UIColor! = UIColor(red: 164/255, green: 57/255, blue: 48/255, alpha: 1.0)
    var colorForFill: UIColor! = UIColor.white
    var lineWidth = 1
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        let w = self.frame.size.width
        let h = self.frame.size.height
        
        let rectangle = CGRect(x: 2, y: 2, width: w-4, height: h-4)
        let rectPath = UIBezierPath(roundedRect: rectangle, cornerRadius: 4)
        colorForFill.setFill()
        colorForBezel.setStroke()
        rectPath.lineWidth = CGFloat(lineWidth)
        rectPath.stroke()
        rectPath.fill()
    }
}
