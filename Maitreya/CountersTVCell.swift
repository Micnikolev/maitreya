//
//  CountersTVCell.swift
//  Maitreya
//
//  Created by Michael Nikolaev on 06.09.17.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import UIKit

class CountersTVCell: UITableViewCell {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var roundSizeLbl: UILabel!
    
    var delegate: CounterDelegate!
    
    var counter: Counter! {
        didSet {
            nameLbl.text = counter.name
            roundSizeLbl.text = counter.roundSize
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
