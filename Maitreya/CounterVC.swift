//
//  CounterVC.swift
//  Maitreya
//
//  Created by Michael Nikolaev on 31.08.17.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import UIKit
import RealmSwift

class Counter: Object {
    dynamic var roundSize: String!
    dynamic var name: String!
}

protocol CounterDelegate {
    func deleteCounter(counter: Counter)
}

class CounterVC: UIViewController, UITableViewDelegate, UITableViewDataSource, CounterDelegate, AddCounterDelegate {

//    @IBOutlet weak var roundedView: RoundedView!
    @IBOutlet weak var mainTV: UITableView!
    @IBOutlet weak var bottomBarView: UIImageView!
    @IBOutlet weak var addCounterBtn: UIButton!
    
    var counters = [Counter]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let realm = try! Realm()
        let cntr = realm.objects(Counter.self)
        
        if cntr.count == 0 {
            counters.append(Counter(value: ["name": "Basic Counter", "roundSize": "108"]))
            counters.append(Counter(value: ["name": "Daily Practice", "roundSize": "7"]))
            counters.append(Counter(value: ["name": "Basic Counter", "roundSize": "108"]))
            counters.append(Counter(value: ["name": "Weekly Practice", "roundSize": "216"]))
            counters.append(Counter(value: ["name": "Daily Practice", "roundSize": "7"]))
            
            try! realm.write {
                realm.add(counters)
            }
        } else {
            for cn in cntr {
                counters.append(cn)
            }
        }
        
        mainTV.register(UINib(nibName: "CountersTVCell", bundle: nil), forCellReuseIdentifier: "CountersCell")
        
        bottomBarView.tintColor = UIColor(red: 124/255, green: 20/255, blue: 44/255, alpha: 1.0)
        
        mainTV.delegate = self
        mainTV.dataSource = self
        
        mainTV.tableFooterView = UIView()
        
        addCounterBtn.setImage(UIImage(named: "iconAddCounterOn"), for: .highlighted)
        
//        roundedView.colorForBezel = UIColor(red: 87/255, green: 86/255, blue: 88/255, alpha: 1.0)
//        roundedView.lineWidth = 4
    }
    
    func saveCounter(name: String, roundSize: String) {
        counters.append(Counter(value: ["name": name, "roundSize": roundSize]))
        mainTV.reloadSections(IndexSet([0]), with: .fade)
        
    }
    
    func deleteCounter(counter: Counter) {
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return counters.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountersCell", for: indexPath) as? CountersTVCell
        cell?.counter = counters[indexPath.row]
        cell?.delegate = self
        
        return cell!
    }
    
    @IBAction func addCounter(_ sender: Any) {
        let controller = AddCounterVC(nibName: "AddCounterVC", bundle: nil)
        controller.delegate = self
//        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated: true, completion: nil)
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let controller = CounterDetailsVC(nibName: "CounterDetailsVC", bundle: nil)
        self.present(controller, animated: false, completion: nil)
    }
    
    //Navigation

    @IBAction func pressedFirst(_ sender: Any) {
        let controller = TimerVC(nibName: "TimerVC", bundle: nil)
        self.present(controller, animated: false, completion: nil)
    }
    
    @IBAction func pressedThird(_ sender: Any) {
        let controller = PracticeVC(nibName: "PracticeVC", bundle: nil)
        self.present(controller, animated: false, completion: nil)
    }
    
    @IBAction func pressedForth(_ sender: Any) {
        let controller = PresetsVC(nibName: "PresetsVC", bundle: nil)
        self.present(controller, animated: false, completion: nil)
    }
}
