//
//  PracticeVC.swift
//  Maitreya
//
//  Created by Michael Nikolaev on 23.09.17.
//  Copyright © 2017 Michael Nikolaev. All rights reserved.
//

import UIKit

class PracticeVC: UIViewController {

    @IBOutlet weak var secondBtn: UIButton!
    @IBOutlet weak var bottomBarView: UIImageView!
    @IBOutlet weak var timer: AppusCircleTimer!
    @IBOutlet weak var timerLbl: UILabel!
    
    var midViewX = CGFloat()
    var midViewY = CGFloat()
    
    var circlePath2 = UIBezierPath()
    var shapeLayer2 = CAShapeLayer()
    
    var time = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        timer.elapsedTime = 20
        timer.totalTime = 10000
        timer.start()
        bottomBarView.tintColor = UIColor(red: 124/255, green: 20/255, blue: 44/255, alpha: 1.0)
        
        midViewX = view.frame.midX
        midViewY = view.frame.midY-50
        // Do any additional setup after loading the view, typically from a nib.
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: midViewX,y: midViewY), radius: CGFloat(100), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = UIColor(red: 208/255, green: 202/255, blue: 188/255, alpha: 1.0).cgColor
        shapeLayer.lineWidth = 25
        view.layer.addSublayer(shapeLayer)
        
        let angleEarth: Double = 0
        let angleEarthAfterCalculate: CGFloat = CGFloat(angleEarth*Double.pi/180) - CGFloat(Double.pi/2)
        let earthX = midViewX + cos(angleEarthAfterCalculate)*100
        let earthY = midViewY + sin(angleEarthAfterCalculate)*100
        circlePath2 = UIBezierPath(arcCenter: CGPoint(x: earthX,y: earthY), radius: CGFloat(10), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
        shapeLayer2.path = circlePath2.cgPath
        shapeLayer2.fillColor = UIColor(red: 124/255, green: 20/255, blue: 44/255, alpha: 1.0).cgColor
        shapeLayer2.strokeColor = UIColor.clear.cgColor
        shapeLayer2.lineWidth = 7
        view.layer.addSublayer(shapeLayer2)
        
        let dragBall = UIPanGestureRecognizer(target: self, action:#selector(dragBall(recognizer:)))
        view.addGestureRecognizer(dragBall)
    }
    
    var ballX = 0
    
    func dragBall(recognizer: UIPanGestureRecognizer) {
        let point = recognizer.location(in: self.view);
        let earthX = Double(point.x)
        let earthY = Double(point.y)
        let midViewXDouble = Double(midViewX)
        let midViewYDouble = Double(midViewY)
        let angleX = (earthX - midViewXDouble)
        let angleY = (earthY - midViewYDouble)
        //let angle = tan(angleY/angleX)
        let angle = atan2(angleY, angleX)
        let earthX2 = midViewXDouble + cos(angle)*100
        let earthY2 = midViewYDouble + sin(angle)*100
        circlePath2 = UIBezierPath(arcCenter: CGPoint(x: earthX2,y: earthY2), radius: CGFloat(10), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
        shapeLayer2.path = circlePath2.cgPath
        
        print(angle)
        
        
        time += 0.3
        var minutes = Int(time) / 60
        var seconds = Int(time)
     
        timerLbl.text = ""
        if minutes < 10 {
            timerLbl.text = "0"
        }
        
        let timeInSec = time.truncatingRemainder(dividingBy: 60)
        
        timerLbl.text = timerLbl.text! + "\(minutes)"+":"
        
        if seconds > 59 {
            seconds = 0
        }
        
        if seconds < 10 {
            timerLbl.text = timerLbl.text! + "0"
        }
        
        timerLbl.text = timerLbl.text! + "\(seconds)"
        
    }
    
    //Navigation
    
    @IBAction func pressedFirst(_ sender: Any) {
        let controller = TimerVC(nibName: "TimerVC", bundle: nil)
        self.present(controller, animated: false, completion: nil)
    }
    
    @IBAction func pressedThird(_ sender: Any) {
        let controller = CounterVC(nibName: "CounterVC", bundle: nil)
        self.present(controller, animated: false, completion: nil)
    }
    
    @IBAction func pressedForth(_ sender: Any) {
        let controller = PresetsVC(nibName: "PresetsVC", bundle: nil)
        self.present(controller, animated: false, completion: nil)
    }

}
